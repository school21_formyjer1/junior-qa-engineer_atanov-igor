### 5-7 объектов из "Local Storage" и "Session Storage" в виде таблицы: Ключ / Значение / Краткое описание


1. ### www.youla.ru
|База данных|Ключ (key)|Значение (Value)|Краткое описание|
|-|-|-|-|
|Local Storage|mmr.eventData.uuid:ec61dc435ab5982288366bafe97cbcf|3d1bfa55-307a-43db-95fc-3e16c57dfe82|данные о конкретном событии|
|Session Storage|excludingBanners|["138612805","141722981"]|Блокировщики баннеров сайта на данный момент|

![clipboard](https://imgur.com/3zaPak7.png)

![clipboard](https://imgur.com/udjUdjI.png)


2. ### https://Wildberries.ru/

|База данных|Ключ (key)|Значение (Value)|Краткое описание|
|-|-|-|-|
|Local Storage|wb__cargo_data_dt|1688243468369|данные отгрузок|
|Session Storage|spa_history|[{"state":{"name":"SpaHomeEntrypoint","id":"affba655-81db-a9d3-94e6-b6d68d8f05a2","title":""}}]	|История посещения страниц сайта в этот раз|

![clipboard](https://imgur.com/zVT8naE.png)

![clipboard](https://imgur.com/slV0oDB.png)

3. ### https://www.lamoda.ru/
|База данных|Ключ (key)|Значение (Value)|Краткое описание|
|-|-|-|-|
|Local Storage|affiliate_nl_not_affected|{"date":1682067114,"medium":"cpc","source":"YDirect","campaign":"3|Региональная категория одежды|
|Session Storage|__ym_tab_guid|[d1209e0b-1fd7-9c61-2638-0efb74afd9f3|Идентификатор вкладки сайта в этот раз|

![clipboard](https://imgur.com/hHoYtWD.png)

![clipboard](https://imgur.com/Zh9333O.png)

4. ### www.vk.ru

|База данных|Ключ (key)|Значение (Value)|Краткое описание|
|-|-|-|-|
|Local Storage|reforged-storage-db-v1-200800399-theme|	"system"|Вкладки левой боковой панели |
|Session Storage|STATS_NAVIGATION_SCREEN|wall|Статус навигации главного экрана сайта|

![clipboard](https://imgur.com/Fvp2AtP.png)

![clipboard](https://imgur.com/wun7z3s.png)

5. ### https://imgur.com/a/D48iVKU

|База данных|Ключ (key)|Значение (Value)|Краткое описание|
|-|-|-|-|
|Session Storage|cs_smart_adserver|6461677657629395449|Сервер интелектуальной рекламы|
|Local Storage|guestUserAlbums|["IddRM2wRl6n3KDi","tJCZasd0FmEimkI","Q9gnwHJv17kXJCM"]|список посетителей в данный момент |


![clipboard](https://imgur.com/yHCQ8Ov.png)

![clipboard](https://imgur.com/N1QlQEZ.png)
### Объекты веб-хранилища, созданные с помощью метода setItem(key, value) в sessionStorage и в localStorage. 

<image src="/misc/images/LocalStorage1.png" alt="Local Storage 1"> 

<image src="/misc/images/LocalStorage2.png" alt="Local Storage 2">

<image src="/misc/images/SessionStorage1.png" alt="Session Storage 1">

<image src="/misc/images/SessionStorage2.png" alt="Session Storage 2">

#
[Local Storage 1](misc/images/LocalStorage1.png)

[Local Storage 2](misc/images/LocalStorage2.png)

[Session Storage 1](misc/images/SessionStorage1.png)

[Session Storage 2](misc/images/SessionStorage2.png)