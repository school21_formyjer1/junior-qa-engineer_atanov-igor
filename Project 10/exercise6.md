<h3 id="задание-6-создание-sql-запросов">Задание №6. Создание SQL-запросов</h3>

**Customers**

Пример 1. 

SQL-запроса с использованием выражения **LIMIT** для ограничения записей:
> [Operator LIMIT](https://imgur.com/a0VS5Uz.png)

Пример 2. 

SQL-запроса с использованием выражения **LIKE** для выбора ограниченных записей:
> [Operator LIKE](https://imgur.com/v7GS3Td.png)

Пример 3. 

SQL-запроса с использованием выражения **OR** для выбора нескольких записей:
> [Operator OR](https://imgur.com/IROYCWH.png)

**Categories**	

Пример 4. 

SQL-запроса с использованием выражения **DELETE** для удаления записей:
> [Operator DELETE](https://imgur.com/TyNWUAp.png)

Пример 5. 

SQL-запроса с использованием выражения **ASC** для сортировки записей по алфавиту:
> [Operator ASC](https://imgur.com/K8OmwG7.png)

Пример 6. 

SQL-запроса с использованием выражения **BETWEEN** для сортировки записей по алфавиту:
> [Operator BETWEEN](https://imgur.com/beS3g0s.png)

**Employees**	

Пример 7. 

SQL-запроса с использованием выражения **WHERE** для огранниченного выбора значений:
> [Operator WHERE](https://drive.google.com/file/d/1zeRoqzPSYJvMGv3KV6vdhDO2qBx2ByDu/view?usp=drive_link)

Пример 8. 

SQL-запроса с использованием выражения **AND** для детализированного запроса:
> [Operator AND](https://drive.google.com/file/d/1M1Dtv16khqkKkjcaUrGYlfAPsjFajuD6/view?usp=drive_link)

Пример 9. 

SQL-запроса с использованием выражения **LIKE** для выбора ограниченных записей:
> [Operator LIKE](https://drive.google.com/file/d/1QPr8Q1eCew_uf-PQc9YYQOvp4hyHtLhA/view?usp=drive_link)

**OrderDetails**

Пример 10. 

SQL-запроса с использованием выражения **DESC** для сортировки результата в порядке убывания:
> [Operator DESC](https://drive.google.com/file/d/12xPtIsj8PXWqu-EgZicRbO_A_LoBeOSQ/view?usp=drive_link)

Пример 11. 

SQL-запроса с использованием выражения **INSERT INTO** для добавления значений:
> [Operator INSERT INTO](https://drive.google.com/file/d/1y8TkznSNOrecZHMPCVU0ZpBQdxrcnKPT/view?usp=drive_link)

Пример 12. 

SQL-запроса с использованием выражения **OR** для детализации запроса:
> [Operator OR](https://drive.google.com/file/d/1M2XHeckUODcmRKg9StTxm6FvfbQFJKz2/view?usp=drive_link)

**Orders**	

Пример 13. 

SQL-запроса с использованием выражения **SET** для выбора данных из запроса:
> [Operator SET](https://drive.google.com/file/d/1GHCv8cx7TXzYStPfloMyiNESIwJsU06_/view?usp=drive_link)

Пример 14. 

SQL-запроса с использованием выражения **DELETE** для удаления записей:
> [Operator DELETE](https://drive.google.com/file/d/1w7PzB8krPYzvLsLzq9FatFSDOcOOxAHR/view?usp=drive_link)

Пример 15. 

SQL-запроса с использованием выражения **AND** для объединения записей:
> [Operator AND](https://drive.google.com/file/d/1opod6gBAWz-juoLaydgIVdih1Q-HhBSh/view?usp=drive_link)

**Products**

Пример 16.

SQL-запроса с использованием выражения **WHERE** для сортировки результата > 100:
> [Operator in The WHERE Clause](https://drive.google.com/file/d/1wiRoRDVLJ2bLlfmGZ3CnyZv-tVeFrV-d/view?usp=drive_link)

Пример 17.

SQL-запроса с использованием выражения **ORDER BY** для сортировки результата в порядке убывания:
> [Operator in ORDER BY DESC](https://drive.google.com/file/d/1z0CJFDKRBeArEET06MCGOIJ7zFxg1SGT/view?usp=drive_link)

Пример 18.

SQL-запроса с использованием выражения **UPDATE** для обновления нескольких записей:
> [Operator in UPDATE Multiple Records](https://drive.google.com/file/d/1roSiX8ok3N9WRdUMaXbO9ODipcunAjgp/view?usp=drive_link)

**Shippers**	

Пример 19. 

SQL-запроса с использованием выражения **UNION** для объединения данных их разных таблиц :
> [Operator UNION](https://drive.google.com/file/d/1rRJyIwUgCDq8YpJwziTg1qPi6pKsrf45/view?usp=drive_link)

Пример 20. 

SQL-запроса с использованием выражения **DELETE** для удаления записей:
> [Operator DELETE](https://drive.google.com/file/d/1kpohWImVz6rVo1yV5jGlHTe4N7fSFlkK/view?usp=drive_link)

Пример 21. 

SQL-запроса с использованием выражения **ORDER BY** для :
> [Operator ORDER BY](https://drive.google.com/file/d/16DQB9hOo9907v6_H8uD-txIuusrrv3uP/view?usp=drive_link)

**Suppliers**	

Пример 22. 

SQL-запроса с использованием выражения **DISTINCT** для выбора различных значений:
> [Operator DISTINCT](https://drive.google.com/file/d/1EPuJE2rr5Ne8rRh0wok8ynIyB8D0ekN-/view?usp=drive_link)

Пример 23. 

SQL-запроса с использованием выражения **BETWEEN** для сортировки данных:
> [Operator BETWEEN](https://drive.google.com/file/d/1xxc6_DxGuONlZuwGnPH7nttLcsVJiKEW/view?usp=drive_link)

Пример 24. 

SQL-запроса с использованием выражения **COUNT** для извлечения количества уникальных стран из таблицы "Customers" :
> [Operator COUNT](https://drive.google.com/file/d/1VQg6tv7aZCkB326lSv6g097sOXHvLSys/view?usp=drive_link)


