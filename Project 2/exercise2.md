# Список тестовых сценариев 
|№|Название теста|Шаг|Шаг тестирования|Тестовые данные|
|:-:|:---------------:|:--:|------|-----|
| Сценарий №1   | Успешное прохождение авторизации|1|Открыть сайт https://www.saucedemo.com|https://www.saucedemo.com
|||2|Вводим валидные данные в поле "Username" |standard_user
|||3|Вводим валидные данные в поле "Password" |secret_sauce
|||4|Кликнуть "LOGIN"| Курсор мыши + ЛКМ
|Сценарий №2.|Добавление товаров в корзину | 1| __На главной странице сайта выбрать любой товар__|Курсор мыши
|||1.1|Кликнуть "Add to cart"| ЛКМ
|||2| __Перейти в карточку товара на главной странице сайта__|Курсор мыши "Наименование товара" или "Фото товара"
|||2.1|Кликнуть "Add to cart"| ЛКМ
|Сценарий №3|Удаление товаров из корзины|1|__На главной странице сайта нажать иконку "Корзина"__| ЛКМ
|||1.1|Выбрать товар|Курсор мыши
|||1.2|Кликнуть "Remove"| ЛКМ
|||2| __Перейти в карточку товара на странице Корзина__|Курсор мыши "Наименование товара"
|||2.1|Кликнуть "Remove"| ЛКМ
|||3| __На главной странице сайта выбрать добавленный товар__|Курсор мыши
|||3.1|Кликнуть "Remove"| ЛКМ
|||4| __Перейти в карточку товара на главной странице сайта__|Курсор мыши "Наименование товара" или "Фото товара"
|||4.1|Кликнуть "Remove"| ЛКМ
|Сценарий №4|Проверка ссылок на странице в боковом меню|1|__На главной странице сайта нажать иконку Бокового меню__|Курсор мыши + ЛКМ
|||1.1|Кликнуть All Items|Курсор мыши + ЛКМ
|||1.2|Кликнуть About|Курсор мыши + ЛКМ
|||1.3|Кликнуть Logout|Курсор мыши + ЛКМ
|||1.4|Кликнуть Reset App State|Курсор мыши + ЛКМ
|||2|__На странице сайта Корзина нажать иконку Бокового меню__|Курсор мыши + ЛКМ
|||2.1|Кликнуть All Items|Курсор мыши + ЛКМ
|||2.2|Кликнуть About|Курсор мыши + ЛКМ
|||2.3|Кликнуть Logout|Курсор мыши + ЛКМ
|||2.4|Кликнуть Reset App State|Курсор мыши + ЛКМ
|||3|__На странице сайта Карточка товара нажать иконку Бокового меню__|Курсор мыши + ЛКМ
|||3.1|Кликнуть All Items|Курсор мыши + ЛКМ
|||3.2|Кликнуть About|Курсор мыши + ЛКМ
|||3.3|Кликнуть Logout|Курсор мыши + ЛКМ
|||3.4|Кликнуть Reset App State|Курсор мыши + ЛКМ
|Сценарий №5|Проверка ссылок на странице в футере|1|Кликнуть иконку ссылки  https://twitter.com/saucelabs|Курсор мыши + ЛКМ
|||2|Кликнуть иконку ссылки https://www.facebook.com/saucelabs|Курсор мыши + ЛКМ
|||3|Кликнуть иконку ссылки https://www.linkedin.com/company/sauce-labs/|Курсор мыши + ЛКМ
|Сценарий №6|Сортировка товаров|1|__Сортировка товара (Name A to Z)__|Курсор мыши
|||1.1|Навести курсор на вкладку Сортировка товаров | Курсор мыши
|||1.2| Кликнуть на вкладку Сортировка товаров|ЛКМ
|||1.3|В открывшемся модольном окне, из списка выбрать (Name A to Z)| Курсоро мыши+ ЛКМ
|||1.4|Кликнуть Name A to Z|ЛКМ
|||2|__Сортировка товара (Name Z to A)__|Курсор мыши
|||2.1|Навести курсор на вкладку Сортировка товаров | Курсор мыши
|||2.2| Кликнуть на вкладку Сортировка товаров|ЛКМ
|||2.3|В открывшемся модольном окне, из списка выбрать (Name Z to A)| Курсоро мыши+ ЛКМ
|||2.4|Кликнуть Name Z to A|ЛКМ
|||3|__Сортировка товара (Price (low to high)__|Курсор мыши
|||3.1|Навести курсор на вкладку Сортировка товаров | Курсор мыши
|||3.2| Кликнуть на вкладку Сортировка товаров|ЛКМ
|||3.3|В открывшемся модольном окне, из списка выбрать (low to high)| Курсоро мыши+ ЛКМ
|||3.4|Кликнуть (low to high)|ЛКМ
|||4|__Сортировка товара (Price (high to low)__|Курсор мыши
|||4.1|Навести курсор на вкладку Сортировка товаров | Курсор мыши
|||4.2| Кликнуть на вкладку Сортировка товаров|ЛКМ
|||4.3|В открывшемся модольном окне, из списка выбрать (high to low)| Курсоро мыши+ ЛКМ
|||4.4|Кликнуть (high to low)|ЛКМ
|Сценарий №7| Работа с корзиной|1|__На главной странице сайта кликнуть иконку "Корзина"__|Курсор мыши + ЛКМ
|||2|Кликнуть "Checkout"| Курсор мыши + ЛКМ
|||3|Кликнуть "Continue Shopping|Курсор мыши + ЛКМ
|||4|Нажать иконку Бокового меню|Курсор мыши + ЛКМ
|Сценарий №8|Оформление заказов|1|__На главной странице сайта кликнуть иконку "Корзина"__|Курсор мыши + ЛКМ
|||2|Кликнуть "Checkout"|Курсор мыши + ЛКМ
|||3|Ввести валидные данные в поле "FirstName"|standard_user
|||4|Ввести валидные данные в поле "Last Name"|standard_user
|||5|Ввести валидные данные в поле "Zip\Postal Code"|standard_user
|||6|Кликнуть кнопку Continue|Курсор мыши + ЛКМ
|||7|Кликнуть кнопку Finish|курсор мыши + ЛКМ
|||8|Кликнуть кнопку Back Home|курсор мыши + ЛКМ