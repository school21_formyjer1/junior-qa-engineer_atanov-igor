### Описание значений, которые могут принимать поля на странице авторизации https://www.saucedemo.com

|"Username"|"Password"|
|-|-|
|пустое|пустое|валидный secret_sauce|
|валидный standard_user|валидный|
|валидный locked_out_user|невалидный|
|валидный problem_user|
|валидный performance_glitch_user|
|невалидный|
Итого: 6*3=18

### Список комбинаций значений влияющих на работу

|№|"Username"|"Password"|
|-|-|-|
|1|пустой|  пустое
|2|пустой|  валидный secret_sauce
|3|пустой|  невалидный
|4|валидный standard_user|  пустое
|5|валидный standard_user|  валидный secret_sauce
|6|валидный standard_user|  невалидный
|7|валидный locked_out_user|  пустое
|8|валидный locked_out_user|	валидный secret_sauce
|9|валидный locked_out_user|	невалидный
|10|валидный problem_user|	пустое
|11|валидный problem_user|	валидный secret_sauce
|12|валидный problem_user|	невалидный
|13|валидный performance_glitch_user|	пустое
|14|валидный performance_glitch_user|	валидный secret_sauce
|15|валидный performance_glitch_user|	невалидный
|16|невалидный|	пустое
|17|невалидный|	валидный secret_sauce
|18|невалидный|	невалидный

### Список тест-кейсов которые проверяют сразу два значения

№|"Username"|"Password"|Папарное тестирование
|-|-|-|-|
|1|пустое|пустое
|2|пустой|	валидный secret_sauce|	невалидный
|3|standard_user|	пустое|	 locked_out_user/ problem_user/ performance_glitch_user/невалидный
|4|standard_user|	валидный secret_sauce|	
|5|standard_user|	невалидный|	 locked_out_user/ problem_user/ performance_glitch_user/невалидный
|6|locked_out_user|	валидный secret_sauce|	
|7|problem_user|	валидный secret_sauce|	
|8|performance_glitch_user|	валидный secret_sauce	
|9|невалидный|	валидный secret_sauce	

### Тестовый набор (Test Suite): Авторизация на сайте https://www.saucedemo.com 

|Autor:<br> И.С.Атанов| Spec ID: Project III / Задание III| Priority: 1|Produser: Е.К.Опарина|Developer: С.С.Гончаров|
|:-|-|-|-|-|
|__OVERVIEW:__|Данный тест комплект проверяет поля - "Username" и "Password|
|__GLOBAL SETUP and ADDITIONAL INFO:__|1. Адрес сайта https://www.saucedemo.com;<br> 2. Валидные данные UserName и Password можно посмотреть в футоре на странице авторизации;<br>3.Тест набор на основе папарного тестирования.

|TC ID| P 3.3.01| Priority: 1|
|-|-|-|
|__IDEA (Наименование)__|_Поля — "Username" и "Password" пустные_
|__Additional info__|1.Открыть сайт https://www.saucedemo.com<br>2.Валидные Username:<br> а) ___standard_user___<br> б) ___locked_out_user___<br>в) ___problem_user___<br>г) ___performance_glitch_user___ <br>3.Валидный пароль: __secret_sauce__
__Executio part__|___PROCEDURE(шаги)___|___EXPECTED RESULT (ожидаемый результат)___|
||1.Оба поля оставить пустыми<br>2.Кликнуть кнопку Login|Сообщение об ошибке: Epic sadface: Username is required

|TC ID| P 3.3.02| Priority: 1|
|-|-|-|
|__IDEA (Наименование)__|_пустой/валидный secret_sauce_
|__Additional info__|1.Открыть сайт https://www.saucedemo.com<br>2.Валидные Username:<br> а) ___standard_user___<br> б) ___locked_out_user___<br>в) ___problem_user___<br>г) ___performance_glitch_user___ <br>3.Валидный пароль: __secret_sauce__
__Executio part__|___PROCEDURE(шаги)___|___EXPECTED RESULT (ожидаемый результат)___|
||1.Поле Username оставить пустыми<br>2.В поле для ввода пароля ввести валидный пароль: secret_sauce<br>3.Кликнуть кнопку Login|Сообщение об ошибке: Epic sadface: Username is required

|TC ID| P 3.3.03| Priority: 1|
|-|-|-|
|__IDEA (Наименование)__|_standard_user/пустое_
|__Additional info__|1.Открыть сайт https://www.saucedemo.com<br>2.Валидные Username:<br> а) ___standard_user___<br> б) ___locked_out_user___<br>в) ___problem_user___<br>г) ___performance_glitch_user___ <br>3.Валидный пароль: __secret_sauce__
__Executio part__|___PROCEDURE(шаги)___|___EXPECTED RESULT (ожидаемый результат)___|
||1.В поле Username ввести standard_user<br>2.Поле для ввода пароля оставить пустым<br>3.Кликнуть кнопку Login|Сообщение об ошибке: Epic sadface: Password is required
 
|TC ID| P 3.3.04| Priority: 1|
|-|-|-|
|__IDEA (Наименование)__|_standard_user/валидный secret_sauce_
|__Additional info__|1.Открыть сайт https://www.saucedemo.com<br>2.Валидные Username:<br> а) ___standard_user___<br> б) ___locked_out_user___<br>в) ___problem_user___<br>г) ___performance_glitch_user___ <br>3.Валидный пароль: __secret_sauce__
__Executio part__|___PROCEDURE(шаги)___|___EXPECTED RESULT (ожидаемый результат)___|
||1.В поле Username ввести standard_user<br>2.В поле для ввода пароля ввести:secret_sauce <br>3.Кликнуть кнопку Login|Успешная авторизация

|TC ID| P 3.3.05| Priority: 1|
|-|-|-|
|__IDEA (Наименование)__|_standard_user/невалидный_
|__Additional info__|1.Открыть сайт https://www.saucedemo.com<br>2.Валидные Username:<br> а) ___standard_user___<br> б) ___locked_out_user___<br>в) ___problem_user___<br>г) ___performance_glitch_user___ <br>3.Валидный пароль: __secret_sauce__
__Executio part__|___PROCEDURE(шаги)___|___EXPECTED RESULT (ожидаемый результат)___|
||1.В поле Username ввести standard_user<br>2.В поле для ввода пароля ввести любое невалидное для пароля значение: (например: 123) <br>3.Кликнуть кнопку Login|Сообщениеоб ошибке: Epic sadface: Username and password do not match any user in this service
 
|TC ID| P 3.3.06| Priority: 1|
|-|-|-|
|__IDEA (Наименование)__|_locked_out_user/валидный secret_sauce_
|__Additional info__|1.Открыть сайт https://www.saucedemo.com<br>2.Валидные Username:<br> а) ___standard_user___<br> б) ___locked_out_user___<br>в) ___problem_user___<br>г) ___performance_glitch_user___ <br>3.Валидный пароль: __secret_sauce__
__Executio part__|___PROCEDURE(шаги)___|___EXPECTED RESULT (ожидаемый результат)___|
||1.В поле Username ввести locked_out_user<br>2.В поле для ввода пароля ввести:secret_sauce <br>3.Кликнуть кнопку Login|Сообщениеоб ошибке: Epic sadface: Sorry, this user has been locked out.
 
|TC ID| P 3.3.07| Priority: 1|
|-|-|-|
|__IDEA (Наименование)__|_problem_user/валидный secret_sauce_
|__Additional info__|1.Открыть сайт https://www.saucedemo.com<br>2.Валидные Username:<br> а) ___standard_user___<br> б) ___locked_out_user___<br>в) ___problem_user___<br>г) ___performance_glitch_user___ <br>3.Валидный пароль: __secret_sauce__
__Executio part__|___PROCEDURE(шаги)___|___EXPECTED RESULT (ожидаемый результат)___|_|
||1.В поле Username ввести: problem_user<br>2.В поле для ввода пароля ввести:secret_sauce <br>3.Кликнуть кнопку Login|Успешная авторизация (Обнаружен Bag)
|||__BAG:__ 1.Пользователь авторизован на странице сайта без наполнения<br> 2. На главной странице, фото товара отсутствуют<br>3. С главной страницы доступно добавление только трех товаров<br> 4.Только с одной карточки товара доступно добавление<br> 5.В случае добавления четырех товаров, страница с корзиной становится недоступной<br> 6.Удаление на главной странице и карточки товара из корзины не доступно<br> 7.Цены в карточке товара и главной странице отличаются<br> 8.Наличие некоректных значений цен (символы, отрицательное значение)<br> 8.Сортировка товара не доступна.

|TC ID| P 3.3.08| Priority: 1|
|-|-|-|
|__IDEA (Наименование)__|_performance_glitch_user/валидный secret_sauce_
|__Additional info__|1.Открыть сайт https://www.saucedemo.com<br>2.Валидные Username:<br> а) ___standard_user___<br> б) ___locked_out_user___<br>в) ___problem_user___<br>г) ___performance_glitch_user___ <br>3.Валидный пароль: __secret_sauce__
__Executio part__|___PROCEDURE(шаги)___|___EXPECTED RESULT (ожидаемый результат)___|_|
||1.В поле Username ввести: performance_glitch_user<br>2.В поле для ввода пароля ввести:secret_sauce <br>3.Кликнуть кнопку Login|Успешная авторизация (Обнаружен Bag)
|||__BAG:__ Медленная загрузка/Остановка загрузки(Корзина)

|TC ID| P 3.3.09| Priority: 1|
|-|-|-|
|__IDEA (Наименование)__|_невалидный/валидный secret_sauce_
|__Additional info__|1.Открыть сайт https://www.saucedemo.com<br>2.Валидные Username:<br> а) ___standard_user___<br> б) ___locked_out_user___<br>в) ___problem_user___<br>г) ___performance_glitch_user___ <br>3.Валидный пароль: __secret_sauce__
__Executio part__|___PROCEDURE(шаги)___|___EXPECTED RESULT (ожидаемый результат)___|
||1.В поле Username ввести любое невалидное значение: Например 123<br>2.В поле для ввода пароля ввести:secret_sauce <br>3.Кликнуть кнопку Login|Сообщениеоб ошибке: Epic sadface: Username and password do not match any user in this service