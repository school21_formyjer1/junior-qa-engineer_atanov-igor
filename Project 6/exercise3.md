## Коды ответов

> __Баг-репорты для HTTP Responses Задания 3__

|**ID:** 01| **Заголовок:** Код ответа: 201 неверный|
|-|-|
|**Предусловие:** | База данных содержит сведения работников банка: "name", "birthday", "post"|
|**Ожидаемый результат:**|**Фактический результат:**
|Код ответа `200 Успешный запрос.` | Код ответа  `201 Created — в результате успешного выполнения запроса был создан новый ресурс.`| 
|**Тип бага:** логический  |**Происхождение**: ошибка кодирования|

**Шаги к воспроизведению:**
1. Создать верный запрос (HTTP Requests) списка работников банка; 
2. В стартовой строке использовать метод GET; 
3. В заголовок сообщения включить поле Authorization с верными для аутентификации данными. 
4. Тело запроса содержит: "name", "birthday", "post"
5. Отправить HTTP Requests; 
6. Получить ответ от сервера (HTTP Responses).
------------------------------------------------------------------
 |**ID:** 02| **Заголовок:** Код ответа: 400 неверный|
|-|-|
|**Предусловие:** | База данных содержит сведения работников банка: "name", "birthday", "post"|
|**Ожидаемый результат:**|**Фактический результат:**
|Код ответа `401 Unauthorized — для доступа к запрашиваемому ресурсу требуется аутентификация.` | Код ответа  `400 Bad Request — сервер обнаружил в запросе клиента синтаксическую ошибку.`| 
|**Тип бага:** логический  |**Происхождение**: ошибка кодирования|

**Шаги к воспроизведению:**
1. Создать верный запрос (HTTP Requests) списка работников банка; 
2. В стартовой строке использовать метод GET; 
3. В заголовок сообщения включить поле Authorization с неверными для аутентификации данными. 
4. Тело запроса содержит: "name", "birthday", "post"
5. Отправить HTTP Requests; 
6. Получить ответ от сервера (HTTP Responses).
------------------------------------------------------------------
|**ID:** 03| **Заголовок:** Код ответа: 500 неверный|
|-|-|
|**Предусловие:** | База данных содержит сведения работников банка: "name", "birthday", "post"|
|**Ожидаемый результат:**|**Фактический результат:**
|Код ответа `400 Bad Request — сервер обнаружил в запросе клиента синтаксическую ошибку.` | Код ответа  `500 Internal Server Error — любая внутренняя ошибка сервера, которая не входит в рамки остальных ошибок класса.`| 
|**Тип бага:** логический  |**Происхождение**: ошибка кодирования|

**Шаги к воспроизведению:**
1. Создать запрос (HTTP Requests) списка работников банка; 
2. В стартовой строке использовать метод GET неверного регистра; 
3. В заголовок сообщения включить поле Authorization с верными для аутентификации данными. 
4. Тело запроса содержит: "name", "birthday", "post"
5. Отправить HTTP Requests; 
6. Получить ответ от сервера (HTTP Responses).