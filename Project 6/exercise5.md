# 1. GET ​/api​/v1​/Activities

__HTTP-метод:__ GET

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/Activities

__Заголовки запроса:__ -H  "accept: text/plain; v=1.0"

__Тело запроса:__ отсутствует

__Статус-код ответа:__ 200

__Тело ответа:__
```
[
  {
    "id": 1,
    "title": "Activity 1",
    "dueDate": "2023-06-14T08:48:08.7766634+00:00",
    "completed": false
  },
```


-----------------

# 2. POST ​/api​/v1​/Activities 

__HTTP-метод:__ POST

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/Activities

__Заголовки запроса:__ -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0" -d "

__Тело запроса:__ 
```
{
  "id": 0,
  "title": "string",
  "dueDate": "2023-06-14T08:11:17.306Z",
  "completed": true
}
```
__Статус-код ответа:__ 200

__Тело ответа:__
```
{
  "id": 0,
  "title": "string",
  "dueDate": "2023-06-14T08:11:17.306Z",
  "completed": true
}
```
-----------------------
# 3. POST ​/api​/v1​/Activities

__HTTP-метод:__ POST

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/Activities

__Заголовки запроса:__ -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0" -d "

__Тело запроса:__ 
```
{
  "id": 0,__м__
  "title": "string",
  "dueDate": "2023-06-14T08:11:17.306Z",
  "completed": true
}
```
__Статус-код ответа:__ 400

__Тело ответа:__
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-296d8c5cc38b2e49903aeaccebd4460c-f4a0393c07306b40-00",
  "errors": {
    "$": [
      "'0xD0' is an invalid start of a property name. Expected a '\"'. Path: $ | LineNumber: 1 | BytePositionInLine: 10."
    ]
  }
}
```
-----------------------------------------------------------------
# 4. GET ​/api​/v1​/Activities​/{1}

__HTTP-метод:__ GET

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/Activities/1

__Заголовки запроса:__ -H  "accept: text/plain; v=1.0"

__Тело запроса:__ отсутствует

__Статус-код ответа:__ 200

__Тело ответа:__ 
```
{
  "id": 1,
  "title": "Activity 1",
  "dueDate": "2023-06-14T09:59:02.7930112+00:00",
  "completed": false
}
```
------------------------------------------------------------------
# 5. GET ​/api​/v1​/Activities​/{1908976767}

__HTTP-метод:__ GET

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/Activities/1908976767

__Заголовки запроса:__ -H  "accept: text/plain; v=1.0"

__Тело запроса:__ отсутствует

__Статус-код ответа:__ 404

__Тело ответа:__
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.4",
  "title": "Not Found",
  "status": 404,
  "traceId": "00-15e33aeb2dc73b4994153a26ce652435-f7b7029dda14ef41-00"
}
```
-------------------------------------------------------------------
# 6. PUT ​/api​/v1​/Activities​/{55}

__HTTP-метод:__ PUT

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/Activities/55

__Заголовки запроса:__ -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0" -d "

__Тело запроса:__
```
 {
  "id": 0,
  "title": "string",
  "dueDate": "2023-06-14T08:18:01.271Z",
  "completed": true
}
```
__Статус-код ответа:__ 200

__Тело ответа:__
```
{
  "id": 0,
  "title": "string",
  "dueDate": "2023-06-14T08:18:01.271Z",
  "completed": true
}
```
---------------------
# 7. PUT ​/api​/v1​/Activities​/{909089766554546577879898090}

__HTTP-метод:__ PUT

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/Activities/909089766554546577879898090

__Заголовки запроса:__ -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0" -d "

__Тело запроса:__ 
```
{
  "id": 0,
  "title": "string",
  "dueDate": "2023-06-14T08:18:01.271Z",
  "completed": true
}
```
__Статус-код ответа:__ 400

__Тело ответа:__
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-85b8f0fdb7fee14b9f79285995b96603-aebe03b2f3569044-00",
  "errors": {
    "id": [
      "The value '909089766554546577879898090' is not valid."
    ]
  }
}
```
# 8. DELETE ​/api​/v1​/Activities​/{5}

__HTTP-метод:__ DELETE

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/Activities/5

__Заголовки запроса:__ -H  "accept: */*"

__Тело запроса:__ отсутствует

__Статус-код ответа:__ 200

__Тело ответа:__
```
access-control-allow-origin: * 
 api-supported-versions: 1.0 
 content-length: 0 
 date: Wed14 Jun 2023 09:03:32 GMT 
 server: Kestrel 
```
-----------------
# 9. GET ​/api​/v1​/Authors

__HTTP-метод:__ GET

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/Authors

__Заголовки запроса:__ -H  "accept: text/plain; v=1.0"

__Тело запроса:__ ОТСУТСТВУЕТ

__Статус-код ответа:__ 200

__Тело ответа:__ 
```
[
  {
    "id": 1,
    "idBook": 1,
    "firstName": "First Name 1",
    "lastName": "Last Name 1"
  },
```
-----------------------
# 10. POST /api​/v1​/Authors

__HTTP-метод:__ POST

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/Authors

__Заголовки запроса:__ -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0" -d "

__Тело запроса:__ 
```
{
  "id": 0,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}
```
__Статус-код ответа:__ 200

__Тело ответа:__ 
```
{
  "id": 0,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}
```
# 11. POST /api​/v1​/Authors

__HTTP-метод:__ POST

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/Authors

__Заголовки запроса:__ -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0" -d "

__Тело запроса:__ 
```
{
  "id": 8989998909,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}
```
__Статус-код ответа:__ 400

__Тело ответа:__
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-453c2950a717a44482f3afeddad561b4-d9906d8f857ecf44-00",
  "errors": {
    "$.id": [
      "The JSON value could not be converted to System.Int32. Path: $.id | LineNumber: 0 | BytePositionInLine: 16."
    ]
  }
}
```
-----------------------
# 12. GET ​/api​/v1​/Authors​/authors​/books​/{1}

__HTTP-метод:__ GET

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/Authors/authors/books/

__Заголовки запроса:__  -H  "accept: text/plain; v=1.0"
Request 

__Тело запроса:__ отсутствует

__Статус-код ответа:__ 200

__Тело ответа:__
```
[
  {
    "id": 1,
    "idBook": 1,
    "firstName": "First Name 1",
    "lastName": "Last Name 1"
  },
```
  -----------------------------------
# 13. GET ​/api​/v1​/Authors​/authors​/books​/{1090897590909878}

__HTTP-метод:__ GET

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/Authors/authors/books/

__Заголовки запроса:__  -H  "accept: text/plain; v=1.0"
Request URL

__Тело запроса:__ отсутствует

__Статус-код ответа:__ 400

__Тело ответа:__
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-e199622aa32345478c56a9915c55e07f-18a0f266c19d5846-00",
  "errors": {
    "idBook": [
      "The value '1090897590909878' is not valid."
    ]
  }
}
```
--------------------------
# 14. GET ​/api​/v1​/Authors​/{5}

__HTTP-метод:__ GET

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/Authors/5
__Заголовки запроса:__ -H  "accept: text/plain; v=1.0"

__Тело запроса:__ отсутствует

__Статус-код ответа:__  200

__Тело ответа:__ 
```
{
  "id": 5,
  "idBook": 2,
  "firstName": "First Name 5",
  "lastName": "Last Name 5"
}
```
---------------------------
# 15. GET ​/api​/v1​/Authors​/{50998878768}

__HTTP-метод:__ GET

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/Authors/50998878768

__Заголовки запроса:__ -H  "accept: text/plain; v=1.0"

__Тело запроса:__ отсутствует

__Статус-код ответа:__ 400

__Тело ответа:__
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-10236ca0781c8940a8140967848d15d4-f08f96047f78e945-00",
  "errors": {
    "id": [
      "The value '50998878768' is not valid."
    ]
  }
}
```
-------------------------------
# 16. PUT /api​/v1​/Authors​/{10}

__HTTP-метод:__ PUT

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/Authors/10

__Заголовки запроса:__ -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0" -d "

__Тело запроса:__ 
```
{
  "id": 0,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}
```
__Статус-код ответа:__ 200

__Тело ответа:__
```
{
  "id": 0,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}
```
--------------------
# 17. PUT /api​/v1​/Authors​/{1090908988776}

__HTTP-метод:__ PUT

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/Authors/1090908988776

__Заголовки запроса:__ -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0" -d "

__Тело запроса:__ 
```
{
  "id": 0,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}
```
__Статус-код ответа:__  400

__Тело ответа:__
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-893e2c50d6f8d54db29ab6ebe28a9208-45892167f3406541-00",
  "errors": {
    "id": [
      "The value '1090908988776' is not valid."
    ]
  }
}
```
----------------------
# 18. DELETE ​/api​/v1​/Authors​/{150}

__HTTP-метод:__ DELETE

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/Authors/150

__Заголовки запроса:__ -H  "accept: */*"

__Тело запроса:__ отсутствует

__Статус-код ответа:__ 200

__Тело ответа:__ 
```
access-control-allow-origin: * 
 api-supported-versions: 1.0 
 content-length: 0 
 date: Wed14 Jun 2023 10:24:24 GMT 
 server: Kestrel 
```
------------------
# 19. GET /api​/v1​/Books

__HTTP-метод:__ GET

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/Books

__Заголовки запроса:__ -H  "accept: text/plain; v=1.0"

__Тело запроса:__ отсутствует

__Статус-код ответа:__ 200

__Тело ответа:__
```
[
  {
    "id": 1,
    "title": "Book 1",
    "description": "Lorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\n",
    "pageCount": 100,
    "excerpt": "Lorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\n",
    "publishDate": "2023-06-13T10:27:00.6258218+00:00"
  },
```
--------------------
# 20. POST ​/api​/v1​/Books

__HTTP-метод:__ POST

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/Books

__Заголовки запроса:__ -H  "accept: */*" -H  "Content-Type: application/json; v=1.0" -d "

__Тело запроса:__ 
```
{
  "id": 0,
  "title": "string",
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-06-14T10:29:21.990Z"
}
```
__Статус-код ответа:__ 200

__Тело ответа:__
```
{
  "id": 0,
  "title": "string",
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-06-14T10:29:21.99Z"
}
```
-------------------
# 21. POST ​/api​/v1​/Books

__HTTP-метод:__ POST

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/Books

__Заголовки запроса:__ -H  "accept: */*" -H  "Content-Type: application/json; v=1.0" -d "

__Тело запроса:__ 
```
{
  "id": 0,
  "title": "string",лол
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-06-14T10:29:21.990Z"
}
```
__Статус-код ответа:__ 400

__Тело ответа:__
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-f11e3b462a56fd42a99283a7413b5ad1-67554509196a9a40-00",
  "errors": {
    "$": [
      "'0xD0' is an invalid start of a property name. Expected a '\"'. Path: $ | LineNumber: 2 | BytePositionInLine: 20."
    ]
  }
}
```
-------------------
# 22. GET ​/api​/v1​/Books​/{999}

__HTTP-метод:__ GET

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/Books/999

__Заголовки запроса:__ -H  "accept: text/plain; v=1.0"

__Тело запроса:__ отсутствует

__Статус-код ответа:__ 404

__Тело ответа:__
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.4",
  "title": "Not Found",
  "status": 404,
  "traceId": "00-6c6e4782e981904d8f901eb580747038-6a2ede6fa7148945-00"
}
```
--------------------
# 23. GET ​/api​/v1​/Books​/{50}

__HTTP-метод:__ GET

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/Books/50

__Заголовки запроса:__ -H  "accept: text/plain; v=1.0"

__Тело запроса:__ отсутствует

__Статус-код ответа:__ 200

__Тело ответа:__
```
{
  "id": 50,
  "title": "Book 50",
  "description": "Lorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\n",
  "pageCount": 5000,
  "excerpt": "Lorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\n",
  "publishDate": "2023-04-25T10:35:44.8268115+00:00"
}
```
----------
# 24. PUT ​/api​/v1​/Books​/{56}

__HTTP-метод:__ PUT

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/Books/56

__Заголовки запроса:__ -H  "Content-Type: application/json; v=1.0" -d "

__Тело запроса:__ 
```
{
  "id": 0,
  "title": "string",
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-06-14T10:36:52.778Z"
}
```
__Статус-код ответа:__ 200

__Тело ответа:__
```
{
  "id": 0,
  "title": "string",
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-06-14T10:36:52.778Z"
}
```
----------
# 25. PUT /api​/v1​/Books​/{99999978790909090}

__HTTP-метод:__ PUT

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/Books/99999978790909090

__Заголовки запроса:__ -H  "accept: */*" -H  "Content-Type: application/json; v=1.0" -d "

__Тело запроса:__ 
```
{
  "id": 0,
  "title": "string",
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-06-14T10:36:52.778Z"
}
```
__Статус-код ответа:__ 400

__Тело ответа:__
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-14b385a0acd7de4f802a085a516611fb-c926f3d348b5874a-00",
  "errors": {
    "id": [
      "The value '99999978790909090' is not valid."
    ]
  }
}
```
------------------
# 26. DELETE /api​/v1​/Books​/{7}

__HTTP-метод:__ DELETE

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/Books/7

__Заголовки запроса:__  -H  "accept: */*"

__Тело запроса:__ отсутствует

__Статус-код ответа:__ 200

__Тело ответа:
```
access-control-allow-origin: * 
 api-supported-versions: 1.0 
 content-length: 0 
 date: Wed14 Jun 2023 10:42:04 GMT 
 server: Kestrel 
```
 --------------------

# 27. GET ​/api​/v1​/CoverPhotos

__HTTP-метод:__ GET

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos

__Заголовки запроса:__ -H  "accept: text/plain; v=1.0"
__Тело запроса:__ отсутствует

__Статус-код ответа:__ 

__Тело ответа:__
```
[
  {
    "id": 1,
    "idBook": 1,
    "url": "https://placeholdit.imgix.net/~text?txtsize=33&txt=Book 1&w=250&h=350"
  },
```
  ----------------------
# 28. POST /api​/v1​/CoverPhotos

__HTTP-метод:__ POST

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos

__Заголовки запроса:__ -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0" -d "

__Тело запроса:__ 
```
{
  "id": 0,
  "idBook": 0,
  "url": "string"
}
```
__Статус-код ответа:__ 200

__Тело ответа:__
```
{
  "id": 0,
  "idBook": 0,
  "url": "string"
}
```
---------------------
# 29. POST /api​/v1​/CoverPhotos

__HTTP-метод:__ POST

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos

__Заголовки запроса:__ -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0" -d "

__Тело запроса:__ 
```
{
  "id": 0,1
  "idBook": 0,
  "url": "string"
}  
```
__Статус-код ответа:__ 400

__Тело ответа:__
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-4d4f746bdb943f4e9ce7682ef863b2b0-20d6751adf637542-00",
  "errors": {
    "$": [
      "'1' is an invalid start of a property name. Expected a '\"'. Path: $ | LineNumber: 1 | BytePositionInLine: 10."
    ]
  }
}
```
---------------------

# 30. GET /api​/v1​/CoverPhotos​/books​/covers​/{idBook}

__HTTP-метод:__ GET

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/books/covers/1

__Заголовки запроса:__ -H  "accept: text/plain; v=1.0"

__Тело запроса:__ отсутствует

__Статус-код ответа:__ 200

__Тело ответа:__
```
[
  {
    "id": 1,
    "idBook": 1,
    "url": "https://placeholdit.imgix.net/~text?txtsize=33&txt=Book 1&w=250&h=350"
  }
]
```
---------------------
# 31. GET /api​/v1​/CoverPhotos​/books​/covers​/{167667767676}

__HTTP-метод:__ GET

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/books/covers/167667767676

__Заголовки запроса:__ -H  "accept: text/plain; v=1.0"

__Тело запроса:__ отсутствует

__Статус-код ответа:__ 400

__Тело ответа:__
```
[
  {
    "id": 1,
    "idBook": 1,
    "url": "https://placeholdit.imgix.net/~text?txtsize=33&txt=Book 1&w=250&h=350"
  }
]
```
# 32. GET /api​/v1​/CoverPhotos​/{1}

__HTTP-метод:__ GET

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/1

__Заголовки запроса:__ -H  "accept: text/plain; v=1.0"

__Тело запроса:__ отсутствует

__Статус-код ответа:__ 200

__Тело ответа:__
```
{
  "id": 1,
  "idBook": 1,
  "url": "https://placeholdit.imgix.net/~text?txtsize=33&txt=Book 1&w=250&h=350"
}
```
--------------------------
# 33. GET /api​/v1​/CoverPhotos​/{909897870908}

__HTTP-метод:__ GET

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/909897870908

__Заголовки запроса:__ -H  "accept: text/plain; v=1.0"

__Тело запроса:__ отсутствует

__Статус-код ответа:__ 400

__Тело ответа:__
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-3dbc28d5fef2044aab77ca5e817dbf9c-a7c25ac9121baa46-00",
  "errors": {
    "id": [
      "The value '909897870908' is not valid."
    ]
  }
}
```
-------------------------
# 34. PUT /api​/v1​/CoverPhotos​/{1}

__HTTP-метод:__ PUT

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/1

__Заголовки запроса:__ -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0" -d "

__Тело запроса:__ 
```
{
  "id": 0,
  "idBook": 0,
  "url": "string"
}
```
__Статус-код ответа:__ 200

__Тело ответа:__
```
{
  "id": 0,
  "idBook": 0,
  "url": "string"
}
```
---------------------
# 35. PUT ​/api​/v1​/CoverPhotos​/{1}

__HTTP-метод:__ PUT

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/1

__Заголовки запроса:__ -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0" -d "

__Тело запроса:__ 
```
{
  "id": 0,
  "idBook": 0,
  "url": "string"5
}
```
__Статус-код ответа:__ 400

__Тело ответа:__
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-5b4feea5c06e1148a00157ffe1a67e91-8078592ba94f0846-00",
  "errors": {
    "$": [
      "'5' is invalid after a value. Expected either ',', '}', or ']'. Path: $ | LineNumber: 3 | BytePositionInLine: 17."
    ]
  }
}
```
------------------------------
# 36. DELETE /api​/v1​/CoverPhotos​/{7}

__HTTP-метод:__ DELETE

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/7

__Заголовки запроса:__ -H  "accept: */*"

__Тело запроса:__ отсутствует

__Статус-код ответа:__ 200

__Тело ответа:__
```
access-control-allow-origin: * 
 api-supported-versions: 1.0 
 content-length: 0 
 date: Wed14 Jun 2023 11:00:06 GMT 
 server: Kestrel 
```
 --------------------------
# 37. GET /api​/v1​/Users

__HTTP-метод:__ GET

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/

__Заголовки запроса:__ -H  "accept: text/plain; v=1.0"

__Тело запроса:__ отсутствует

__Статус-код ответа:__ 200

__Тело ответа:__ 
```
[
  {
    "id": 1,
    "userName": "User 1",
    "password": "Password1"
  },
  ```
# 38. POST /api​/v1​/Users

__HTTP-метод:__ POST

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/Users

__Заголовки запроса:__ -H  "accept: */*" -H  "Content-Type: application/json; v=1.0" -d "

__Тело запроса:__
```
 {
  "id": 0,
  "userName": "string",
  "password": "string"
}
```
__Статус-код ответа:__ 200

__Тело ответа:__ 
```
{
  "id": 0,
  "userName": "string",
  "password": "string"
}
```
----------------------
# 39. POST /api​/v1​/Users

__HTTP-метод:__ POST

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/Users

__Заголовки запроса:__ -H  "accept: */*" -H  "Content-Type: application/json; v=1.0" -d "

__Тело запроса:__ 
```
{
  "id": 0,
  
}
```
__Статус-код ответа:__ 

__Тело ответа:__
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-2f532c002ffa9f499c30a69a1c01675c-aa6ee63809ae164b-00",
  "errors": {
    "$": [
      "The JSON object contains a trailing comma at the end which is not supported in this mode. Change the reader options. Path: $ | LineNumber: 3 | BytePositionInLine: 0."
    ]
  }
}
```
-----------------------

# 40. GET /api​/v1​/Users​/{1}

__HTTP-метод:__ GET

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/Users/1

__Заголовки запроса:__ -H  "accept: */*"

__Тело запроса:__ отсутствует

__Статус-код ответа:__ 200

__Тело ответа:__
```
{
  "id": 1,
  "userName": "User 1",
  "password": "Password1"
}
```
-----------------------
# 41. GET /api​/v1​/Users​/{1090898867}

__HTTP-метод:__ GET

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/Users/1090898867

__Заголовки запроса:__ -H  "accept: */*"

__Тело запроса:__ отсутствует

__Статус-код ответа:__ 404

__Тело ответа:__
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.4",
  "title": "Not Found",
  "status": 404,
  "traceId": "00-5d95db7703a34c46ae6928ad42e8de4b-c8da15ab8d812249-00"
}
```

# 42. PUT /api​/v1​/Users​/{5}

__HTTP-метод:__ PUT

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/Users/5

__Заголовки запроса:__ -H  "accept: */*" -H  "Content-Type: application/json; v=1.0" -d "

__Тело запроса:__ 
```
{
  "id": 0,
  "userName": "string",
  "password": "string"
}
```
__Статус-код ответа:__ 200

__Тело ответа:__
```
{
  "id": 0,
  "userName": "string",
  "password": "string"
}
```
# 43. PUT /api​/v1​/Users​/5}

__HTTP-метод:__ PUT

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/Users/5

__Заголовки запроса:__ -H  "accept: */*" -H  "Content-Type: application/json; v=1.0" -d "

__Тело запроса:__ 
```
{
  "id":
  "userName": "string",
  "password": "string"
}
```
__Статус-код ответа:__ 400

__Тело ответа:__
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-1b8e19876462634b9585cfaf6bf548b4-070fd2163c14ac4e-00",
  "errors": {
    "$.id": [
      "The JSON value could not be converted to System.Int32. Path: $.id | LineNumber: 2 | BytePositionInLine: 12."
    ]
  }
}
```
------------------------
# 44. DELETE /api​/v1​/Users​/{5900}

__HTTP-метод:__ DELETE

__Полный URL запроса:__ https://fakerestapi.azurewebsites.net/api/v1/Users/5900

__Заголовки запроса:__ -H  "accept: */*"

__Тело запроса:__ отсутствует

__Статус-код ответа:__ 200

__Тело ответа:__
```
access-control-allow-origin: * 
 api-supported-versions: 1.0 
 content-length: 0 
 date: Wed14 Jun 2023 11:10:38 GMT 
 server: Kestrel 
```
-----------------------------------